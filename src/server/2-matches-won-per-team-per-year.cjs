const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');


let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');
let outputFilePath = path.resolve(__dirname, '../public/output/2-matches-won-per-team-per-year.json')

csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
    let resultMatchesOwnPerTeamPerYear = matchesWonPerTeamPerYear(matchesFile)
    fs.writeFile(outputFilePath, JSON.stringify(resultMatchesOwnPerTeamPerYear), err => {
        if(err){
            throw err;
        }else{
            console.log('Sucess');
        }
    });

})

function matchesWonPerTeamPerYear(matches) {
    return matches.reduce((total, value) => {
        let win = value.winner;
        let season = value.season;
        if (total[season] === undefined) {
            total[season] = {};
        }
        if (total[season][win]) {
            total[season][win]++;
        }
        else {
            total[season][win] = 1;
        }
        return total;
    }, {})
}

