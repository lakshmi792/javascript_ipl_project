const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {

    csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
        let resultStrikeRate = strikeRate(matchesFile, deliveriesFile)
        fs.writeFileSync('../public/output/7-strike-rate-of-batsman-per-each-season.json', JSON.stringify(resultStrikeRate));
    })
})

const strikeRate = (matches, deliveries) => {
    const matchesSeason = matches.reduce((accumulater, match) => {
        accumulater[match.id] = match.season;
        return accumulater;
    }, {});
    //console.log(matchesOfSeason);
    const playersDetails = deliveries.reduce((accumulater, delivery) => {
        let season = matchesSeason[delivery.match_id];
        if (accumulater.hasOwnProperty(delivery.batsman)) {
            if (accumulater[delivery.batsman].hasOwnProperty(season)) {
                accumulater[delivery.batsman][season].totalRuns += parseInt(delivery.total_runs);
                accumulater[delivery.batsman][season].totalBallsFaced += 1;
            }
            else {
                accumulater[delivery.batsman][season] = {};
                accumulater[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
                accumulater[delivery.batsman][season].totalBallsFaced = 1;
            }
        }
        else {
            accumulater[delivery.batsman] = {};
            accumulater[delivery.batsman][season] = {};
            accumulater[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
            accumulater[delivery.batsman][season].totalBallsFaced = 1;
        }
        return accumulater;
    }, {});
   // console.log(playersDetails);
    const result = {};
    Object.keys(playersDetails).map((batsman) => {
        let strike = {};
        Object.keys(playersDetails[batsman]).map(year => {
            let answer = (playersDetails[batsman][year].totalRuns / playersDetails[batsman][year].totalBallsFaced) * 100;
            strike[year] = answer.toFixed(2);
        })
        result[batsman] = strike;
    });
    return result;
    
}

