const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
    let resultBestEconomy = bestEconomyInSuperOvers(deliveriesFile);
   // fs.writeFileSync('../public/output/9-best-economy-in-superovers.json', JSON.stringify(resultBestEconomy));

})

const bestEconomyInSuperOvers = (deliveries) => {
    const economyOfBowlersRun = deliveries.filter((eachBall) => {
        return eachBall.is_super_over !== "0";
    }).reduce((accumulater, current) => {
        if (accumulater[current.bowler]) {
            accumulater[current.bowler]["runs"] += Number(current.total_runs);
            accumulater[current.bowler]["balls"] += 1;
            accumulater[current.bowler]["economic"] = (accumulater[current.bowler]["runs"] / (accumulater[current.bowler]["balls"] / 6)).toFixed(2);
        }
        else {
            accumulater[current.bowler] = {};
            accumulater[current.bowler]["runs"] = Number(current.total_runs);
            accumulater[current.bowler]["balls"] = 1;
        }
        return accumulater;
    }, {});
    // console.log(economyOfBowlersRun);
     
    const economyOfBowler = Object.entries(economyOfBowlersRun).reduce(
        (accumulater, current) => {
            accumulater[current[0]] = current[1].economic;
            return accumulater;
        }, {});

      // console.log(economyOfBowler);
     // let values=Object.entries(economyOfBowler);
     // console.log(values);
      
    const bestBowler = Object.fromEntries(
        Object.entries(economyOfBowler).sort(([, a], [, b]) => [, a][1] - [, b][1]).slice(0, 1)
        
    );
    //return bestBowler;
    //console.log(bestBowler);
};
