const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
    
    csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
        let resultEconomicBowlers = economicBowlers(deliveriesFile, matchesFile)
        fs.writeFileSync('../public/output/4-economic-bowlers.json', JSON.stringify(resultEconomicBowlers));
    })
})
const economicBowlers = (deliveries, matches) => {
    const matchesId = matches.filter((eachMatch) => {
        return eachMatch.season === "2015"
    }).map((each) => each.id);

    //console.log(matchesId);

    const economicBallRuns = deliveries.reduce((accumalater, current) => {
        if (matchesId.includes(current.match_id)) {
            if (accumalater[current.bowler]) {
                let ballCount = Number(current.ball) <= 6 ? 1 : 0;
                accumalater[current.bowler]["runs"] += Number(current.total_runs);
                accumalater[current.bowler]["balls"] += ballCount;
                accumalater[current.bowler]["economic"] = accumalater[current.bowler]["runs"] / (accumalater[current.bowler]["balls"] / 6);
            }
            else {
                accumalater[current.bowler] = {};
                accumalater[current.bowler]["runs"] = Number(current.total_runs);
                accumalater[current.bowler]["balls"] = 1;
            }
        }
        return accumalater;
    }, {});

    //console.log(Object.entries(economicBallRuns));

    //console.log(economicBallRuns)

    const economicOfBowlers = Object.entries(economicBallRuns).reduce((accumalater, current) => {
        accumalater[current[0]] = current[1].economic;
        return accumalater;
    }, {});

    //console.log(economicOfBowlers)

    const result = Object.fromEntries(
        Object.entries(economicOfBowlers).sort(([, a], [, b]) => [, a][1] - [, b][1]).slice(0, 10)
    );

    //console.log(result);
    return result;

}



