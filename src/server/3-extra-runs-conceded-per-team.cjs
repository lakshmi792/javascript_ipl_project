const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {

    csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
        let resultExtraRuns = extraRuns(matchesFile, deliveriesFile)
        fs.writeFileSync('../public/output/3-extra-runs-conceded-per-year.json', JSON.stringify(resultExtraRuns));
    })
})



function extraRuns(matches, deliveries) {
    let matchesId = matches.filter((elements) => {
        return elements.season === '2016'
    }).map((values) => {
        return values.id;
    });

    return extraRunsValues = deliveries.reduce((accumalater, deliveryValues) => {
        if (matchesId.includes(deliveryValues.match_id)) {
            let extraRun = deliveryValues.extra_runs;
            let team = deliveryValues.bowling_team;

            accumalater[team] = accumalater[team] ? accumalater[team] += Number(extraRun) : Number(extraRun);
        }
        return accumalater;
    }, {})
    //console.log(extraRunsValues);

}




