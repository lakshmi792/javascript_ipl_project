const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
    let resultWonTossMatch = wonTossAndMatch(matchesFile)
    fs.writeFileSync('../public/output/5-won-toss-and-match.json', JSON.stringify(resultWonTossMatch));

})

const wonTossAndMatch = (matches) => {
    const teamsWon = matches.reduce((accumulater, match) => {

        if (accumulater[match.toss_winner]) {
            if (match.toss_winner === match.winner) {
                accumulater[match.toss_winner] += 1;
            }
        }
        else {
            if (match.toss_winner === match.winner) {
                accumulater[match.toss_winner] = 1;
            }
        }
        return accumulater;
    }, {});

    return teamsWon;
    //console.log(teamsWon);
}

