const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

//let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
    let resultMatchesPerYear = matchesPerYear(matchesFile)
    fs.writeFileSync('../public/output/1-matches-per-year.json', JSON.stringify(resultMatchesPerYear));

})
function matchesPerYear(matches) {
    return matches.reduce((total, value) => {
        total[value.season] ? total[value.season]++ : total[value.season] = 1
        return total;
    }, {});
}

//module.exports = matchesPerYear;



