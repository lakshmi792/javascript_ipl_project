const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
    let resultHighDismissal = highDismissal(deliveriesFile);
    fs.writeFileSync('../public/output/8-high-dismissal.json', JSON.stringify(resultHighDismissal));

})

const highDismissal = (deliveries) => {
    const batsmanBowler = deliveries.reduce((accumulater, current) => {
        if (current.dismissal_kind !== "" && current.dismissal_kind !== "run out") {
            if (accumulater[current.batsman]) {
                if (accumulater[current.batsman][current.bowler]) {
                    accumulater[current.batsman][current.bowler]++;
                }
                else {
                    accumulater[current.batsman][current.bowler] = 1;
                }
            }
            else {
                accumulater[current.batsman] = {};
                if (accumulater[current.batsman][current.bowler]) {
                    accumulater[current.batsman][current.bowler]++;
                }
                else {
                    accumulater[current.batsman][current.bowler] = 1;
                }
            }
        }
        return accumulater;
    }, {});
    //console.log(batsmanBowler);

    let minimum = 0;
    //let obj=Object.entries(batsmanBowler)
    //console.log(obj);
    let batsmanDismissedByBowler = {};
    Object.entries(batsmanBowler).map((eachBatsman) => {
        const dismissalByBowler = Object.entries(eachBatsman[1]).sort(([, a], [, b]) => [, b][1] - [, a][1]).slice(0, 1);

        //console.log(dismissalByBowler);
        if (dismissalByBowler[0][1] > minimum) {
            minimum = dismissalByBowler[0][1];
            let tempObj = {};
            tempObj[eachBatsman[0]] = dismissalByBowler[0];
            batsmanDismissedByBowler = tempObj;
        }
    });

    return batsmanDismissedByBowler;
    //console.log(batsmanDismissedByBowler);

}
