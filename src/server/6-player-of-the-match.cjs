const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let deliveriesFilePath = path.resolve(__dirname, '../data/deliveries.csv');
let matchesFilePath = path.resolve(__dirname, '../data/matches.csv');

csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
    let resultPlayerOfTheMatch = playerOfTheMatch(matchesFile);
    fs.writeFileSync('../public/output/6-player-of-the-match.json', JSON.stringify(resultPlayerOfTheMatch));

})

const playerOfTheMatch = (matches) => {
    const seasonPlayerOfMatch = matches.reduce((accumulater, current) => {
        if (accumulater[current.season]) {
            if (accumulater[current.season][current.player_of_match]) {
                accumulater[current.season][current.player_of_match]++;
            }
            else {
                accumulater[current.season][current.player_of_match] = 1;
            }
        }
        else {
            accumulater[current.season] = {};
            // accumulater[current.season][current.player_of_match] = 1;
        }
        return accumulater;
    }, {});

    //console.log(seasonPlayerOfMatch);
    //console.log(Object.entries(seasonPlayerOfMatch));

    const playerOfTheMatchEachYear = Object.entries(seasonPlayerOfMatch).reduce(
        (accumulater, eachYear) => {
            let year = Object.entries(eachYear[1]).sort(([, a], [, b]) => [, b][1] - [, a][1]).slice(0,1)
            accumulater[eachYear[0]] = year[0];
            return accumulater;
            //console.log(Object.entries(eachYear[1]));

        }, {});
   // console.log(playerOfTheMatchEachYear)
    return playerOfTheMatchEachYear;
}