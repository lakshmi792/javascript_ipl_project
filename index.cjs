const path = require('path');
const express = require('express');

const problem1 = path.resolve(__dirname, 'src/public/output/1-matches-per-year.json');
const problem2 = path.resolve(__dirname, 'src/public/output/2-matches-won-per-team-per-year.json');
const problem3 = path.resolve(__dirname, 'src/public/output/3-extra-runs-conceded-per-year.json');
const problem4 = path.resolve(__dirname, 'src/public/output/4-economic-bowlers.json');
const problem5 = path.resolve(__dirname, 'src/public/output/5-won-toss-and-match.json');
const problem6 = path.resolve(__dirname, 'src/public/output/6-player-of-the-match.json');
const problem7 = path.resolve(__dirname, 'src/public/output/7-strike-rate-of-batsman-per-each-season.json');
const problem8 = path.resolve(__dirname, 'src/public/output/8-high-dismissal.json');
const problem9 = path.resolve(__dirname, 'src/public/output/9-best-economy-in-superovers.json');
const index = path.resolve(__dirname, 'index.html');

const PORT = process.env.PORT || 4000

let app = express();

app.use(express.static(path.resolve(__dirname)))

app.get('/', (req, res) => {
    res.sendFile(index);
})


app.get('/matches-per-year', (req, res) => {
    res.sendFile(problem1)
})


app.get('/matches-won-per-team-per-year', (req, res) => {
    res.sendFile(problem2);
})

app.get('/extra-runs-per-year', (req, res) => {
    res.sendFile(problem3);
})

app.get('/economic-bowlers', (req, res) => {
    res.sendFile(problem4);
})

app.get('/won-toss-and-match', (req, res) => {
    res.sendFile(problem5);
})

app.get('/player-of-the-match', (req, res) => {
    res.sendFile(problem6);
})

app.get('/stike-rate', (req, res) => {
    res.sendFile(problem7);
})

app.get('/high-dismissal', (req, res) => {
    res.sendFile(problem8);
})

app.get('/best-economy-in-superovers', (req, res) => {
    res.sendFile(problem9);
})

app.listen(PORT,() => {
    console.log(`server is listening on port ${PORT}`);
})